import webapp

from random import randint

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Resource: </label>
        <input type="text" name="resource" required>
      </div>
      <div>
        <label>Content: </label>
        <textarea name="content" rows="3" cols="20" required></textarea>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""
class ContentPostAPP(webapp.Webapp):

    recursos = {"/":"P&aacute;gina de principal",
               "/david":"P&aacute;gina de david"}
    def parse(self, received):
        recibido = received.decode()
        metodo = recibido.split(" ")[0]  # me quedo con el protocolo
        recurso = recibido.split(" ")[1]  # me quedo con la peticion del cliente
        body = recibido.split("\r\n\r\n")[1]  # me quedo con el body

        return metodo, recurso, body #Se envia en forma de tupla

    def process(self, analyzed):
        metodo, recurso, body = analyzed #Se desempaqueta la tupla

        if metodo == "POST":
            recurso_b = "".join(("/",body.split("&")[0].split("=")[1]))
            contenido = body.split("&")[1].split("=")[1]
            self.recursos[recurso_b] = contenido
            print(f"\n\n LOS RECURSOS SON:\n{self.recursos}\n")


        if recurso in self.recursos:
            http = "200 OK"
            html = '<html><body>' \
                   f'<div>El contenido del recurso "{recurso}" es : {self.recursos[recurso]}</div>' \
                   f'<div>{FORM}</div>' \
                   '</body></html>'

        else:
            http = "404 Not Found"
            html = f"<html><body> No se encontro el recurso '{recurso}', pero lo puedes crear" \
                   f"<div>{FORM}</div>" \
                   "</body></html>"

        return http, html


if __name__ == "__main__":
    content = ContentPostAPP("localhost", 1234)
