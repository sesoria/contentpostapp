import socket


class Webapp():

    def __init__(self, ip, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # crear socket IPv4
        mySocket.bind((ip, port))  #
        mySocket.listen(5)  # 5 conexiones simultáneas de TCP que almaceno, a partir de la sexta se desconecta.

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received:")
            received = recvSocket.recv(2048)
            print(received)

            petition = self.parse(received)
            http, html = self.process(petition)

            response = "HTTP/1.1 " + http + "\r\n\r\n" \
                       + html + "\r\n"

            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()
    def parse(self, received):
        recibido = received.decode()
        return recibido.split(" ")[1]  # me quedo con la petición del cliente

    def process(self, analyzed):
        http = "200 OK"
        html = "<html><body><h1>Hello World! Tu peticion es " + analyzed + "</h1>" \
               + "</body></html>"

        return http, html



if __name__ == "__main__":
    webapp = Webapp('localhost', 1236)
